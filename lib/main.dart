import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        //
        primarySwatch: Colors.blue,
      ),
      home:  MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List _Import = [];

  Future<void> readJson() async {
    final String response = await rootBundle.loadString('assets/users.json');
    final data = await jsonDecode(response.toString());
    setState(() {
      _Import = data["Import"];
    });
  }
  @override
  Widget build(BuildContext context) {
    //
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Flutter Read Users JSON'),
      ),
      body: Column(
         children: [
           ElevatedButton(
             child: const Text('Load JSON Data'),
             onPressed: readJson,
           ),
           // Display the data loaded from sample.json
           _Import.isNotEmpty
               ? Expanded(
             child: ListView.builder(
               itemCount: _Import.length,
               itemBuilder: (context, index) {

                 return Card(
                   margin: const EdgeInsets.all(10),
                   child: ListTile(
                     title: Text(_Import[index]["username"]),
                     subtitle:Text(_Import[index]["email"]),
                     leading: CircleAvatar(
                       backgroundImage: NetworkImage(
                         (_Import[index]["urlAvatar"]),
                       ),
                     ),
                   ),
                 );

                 return Container(
                   child: Column(
                     children: [
                       ListTile(
                         title: Text(_Import[index]["username"]),
                         subtitle:Text(_Import[index]["email"]),
                         leading: CircleAvatar(
                           backgroundImage: NetworkImage(
                             (_Import[index]["urlAvatar"]),
                           ),
                         ),
                       ),
                       Divider(),
                     ],
                   ),
                 );
               },
             ),
           )
               : Container()
         ],
      ),
    );
  }
}
